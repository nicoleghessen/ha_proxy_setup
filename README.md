# HA proxy instructions


### Suggested sequence of steps 

- start the 3 machines
- tag them
- use the previous scripts to make 2 nginx machines
- if needed, manually nano index.html file and write SERVER 1 on one machine and SERVER 2 on the other machine.


- Then focus on HA proxy
- First read
- Then try and figure out how to do it manually
- Then automate 
- Better to have everything done automatically but it's something you work towards


### Create 3 instances on AWS
Make 3 ubuntu machines 8g
Include tags
Security Group: with port 80 open to world and port 22 open to you.

### Setup Nginx on 2 of the machines 
To install nginx use the following commands:
```bash
sudo apt update
sudo apt install nginx
```
Once installed change the default pages to be able to identify "Server 1" "Server 2"

##### Edit the Welcome page and swap the default with your own
```bash
$cd /var/www/html
```
- If you $ls at this point you will see "index.nginx-debian.html"

- Now create a new index.html document:
```bash
$ sudo touch index.html
```

- Now you need to open the index.html document and edit the default text:
```bash
$ sudo nano index.html
```

- Input your info and writing and then save and exit $nano:
```bash
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello, Nginx!</title>
</head>
<body>
    <h1>Hello, Nginx!</h1>
    <p>We have just configured our Nginx web server on Ubuntu Server!</p>
</body>
</html>
```


### Now configure the HAproxy:

Logged into your ubuntu machine- to install HAproxy type the command:
```bash 

$ sudo apt install haproxy

```

### Then edit in nano mode to add your server name and the private IP to connect them
```bash
$ sudo nano /etc/haproxy/haproxy.cfg


frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server <server1 name> <private IP 1>:80 check
   server <server2 name> <private IP 2>:80 check
   ```

### Refresh the third machine page and your new welcome page which you edited should appear.
 
 Then restart it and check the status: 

```bash
$ sudo systemctl restart haproxy
$ sudo systemctl status haproxy
```


